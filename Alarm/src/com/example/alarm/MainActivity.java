package com.example.alarm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.example.bc.MyBC;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 闹铃的设置
 * 
 * @author ZSL
 * @version 2014-08-10 11:25:354
 */
public class MainActivity extends Activity {
	/**
	 * 控件
	 */

	private EditText et_date;// 设定时间
	private Button bt_set;// 设定按钮

	/**
	 * 数据
	 */
	String dateString;
	Date date;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();// 初始化

		bt_set.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				dateString = et_date.getText().toString();
				if (dateString.length() == 19) {
					SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					try {
						date=dateFormat.parse(dateString);
						setAlarm(true);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					showToast(MainActivity.this, "清输入真确的日期格式");
				}
			}
		});

	}

	/**
	 * 初始化
	 */
	public void init() {
		et_date = (EditText) findViewById(R.id.alarm_et_date);
		bt_set = (Button) findViewById(R.id.alarm_bt_set);

	}

	/**
	 * 土司
	 */
	public void showToast(Context context, String content) {
		Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
	}

	// 设定闹钟
	public void setAlarm(boolean b) {
		// 获取系统服务
		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
		/**
		 * 定时器主要类型：
		 * 
		 * public static final int ELAPSED_REALTIME //
		 * 当系统进入睡眠状态时，这种类型的闹铃不会唤醒系统。直到系统下次被唤醒才传递它
		 * ，该闹铃所用的时间是相对时间，是从系统启动后开始计时的,包括睡眠时
		 * 间，可以通过调用SystemClock.elapsedRealtime()获得。系统值是3 (0x00000003)。
		 * 
		 * public static final int ELAPSED_REALTIME_WAKEUP
		 * //能唤醒系统，用法同ELAPSED_REALTIME，系统值是2 (0x00000002) 。
		 * 
		 * public static final int RTC
		 * //当系统进入睡眠状态时，这种类型的闹铃不会唤醒系统。直到系统下次被唤醒才传递它，该闹铃所用的时间是绝对时间
		 * ，所用时间是UTC时间，可以通过调用 System.currentTimeMillis()获得。系统值是1 (0x00000001) 。
		 * 
		 * public static final int RTC_WAKEUP //能唤醒系统，用法同RTC类型，系统值为 0
		 * (0x00000000) 。
		 * 
		 * Public static final int POWER_OFF_WAKEUP
		 * //能唤醒系统，它是一种关机闹铃，就是说设备在关机状态下也可以唤醒系统
		 * ，所以我们把它称之为关机闹铃。使用方法同RTC类型，系统值为4(0x00000004)。
		 */
		//注册PendingIntent
		PendingIntent intent=PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(MainActivity.this,MyBC.class), 0);
		if (b) {
			long timeInmillis=date.getTime();
			long timem=System.currentTimeMillis();
			
			Log.i("timeInmillis", timeInmillis+":"+timem);
			Calendar calendar=Calendar.getInstance();
			calendar.setTime(date);
//			alarmManager.set(AlarmManager.RTC_WAKEUP,calendar.getTimeInMillis(), intent);
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 5000, intent);
			showToast(MainActivity.this, timeInmillis+":"+timem+":"+calendar.getTimeInMillis());
//			alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, timeInmillis, 5, intent);
		} else {
			alarmManager.cancel(intent);
		}
	}
}
